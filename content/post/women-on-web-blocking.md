---
title: Women on Web website censored in Spain
date: "2020-05-05"
image: "wow-blocking-post/wow-post-logo.jpg"
author: "Vasilis Ververis, Fadelkon, Ana, Bita, Samba"
tags: "censorship analysis"
---

**Blocked website:** **womenonweb.org**
**Blocking methodologies:** DNS Manipulation, HTTP Blocking, TLS Interception,
TCP resets
**DPI middlebox vendors:** Allot, Fortinet
**Blocked in ISPs:** Vodafone (AS12357 and AS12430), Vodafone Ono (AS6739),
Orange (AS12479 and AS12715), CSUC (AS13041), MÁSMÓVIL (AS15704), XFERA
(AS16299), Telefónica/Movistar (AS3352)

![WoW blocking logo](/wow-blocking-post/wow-post-logo.jpg)

With this article we would like to raise awareness around the increasing level
of web censorship and information control that Spanish Internet Service
Providers (ISPs) have initiated. We will share all the technical details about
the persistent blocking of the Women on Web website by all major Spanish ISPs.

The Women on Web website [womenonweb.org](https://www.womenonweb.org) a
non-profit organization providing support to women and pregnant people has been
blocked by various ISPs all over Spain. The Open Observatory of Network
Interference, [OONI](https://ooni.org/), a global community measuring Internet
censorship, provides tools so that anyone with a network connection can
voluntarily contribute their data to global reports. Recent measurements
indicate that the Women on Web website has been blocked since the end of January
2020 and is still blocked by the majority of the Spanish ISPs as of the time of
this writing.

It is not the first time that the Women on Web website has been blocked. OONI
published [a report in
2019](https://ooni.org/post/2019-blocking-abortion-rights-websites-women-on-wavesweb/)
analysing the confirmed blocking of the Women on Waves and Women on Web websites
in Brazil, Iran, Turkey, South Korea, and Saudi Arabia.

**This is the first time that we observe Women on Web being blocked in Spain.**

In this article we describe how the major ISPs in Spain are blocking
**womenonweb.org**'s website. Spanish ISPs have been blocking this website by
means of DNS manipulation, TCP reset, HTTP blocking with the use of a Deep
Packet Inspection (DPI) infrastructure. Our data analysis is based on network
measurements from OONI data.

## ISP networks by blocking strategy

![Website Censorship Techniques designed by sub.marin.li](/wow-blocking-post/website-censorhip-submarin.png)

### Summary table

| ISP                    | Blocking Methodology                       | DPI      |
| ---------------------- | ------------------------------------------ | -------- |
| Telefónica/Movistar    | DNS Manipulation, HTTP Blocking, TCP Reset | Fortinet |
| Vodafone/Ono           | HTTP Blocking, TLS Interception, TCP Reset | Allot    |
| Orange/Jazztel         | DNS Manipulation                           | -        |
| MASMÓVIL/XFERA         | DNS Manipulation                           | -        |
| CSUC                   | HTTP Blocking, TCP Reset                   | Fortinet |

---
Please find our data analysis (as a CSV file)
[here](/wow-blocking-post/magma-womenonweb-es-ooni-data.csv).

### AS/time graph

The graph illustrates OONI network measurements from the 1st of January 2020
until the 30th of April 2020 of the websites **www.womenonweb.org** and
**www.womenonwaves.org**. On the y-axis of the graph the autonomous system (AS)
network names of each ISP are listed, and on the x-axis the date of the
measurements. The colors of the graph indicate the type of blocking (`dns`,
`http-diff`, `http-failure` and `tcp_ip`), or no blocking indicated in grey.
These types of blocking are described in detail in OONI's [Web Connectivity test
specification](https://github.com/ooni/spec/blob/master/nettests/ts-017-webconnectivity.md#semantics-1).
In the top graph, we see that, from the network measurements,
**www.womenonwaves.org** is not blocked by any ISP in Spain (at least from the
ones we have data on); all measurements have the color grey which means no
blocking is observed. In the bottom graph, the network measurements of the
website **www.womenonweb.org** are illustrated. Here we see a very different
scenario were most ISPs are blocking the website by means of DNS manipulation,
TCP reset, and HTTP blocking with the use of DPI.

![Diagram on different Website Censorship Techniques](/wow-blocking-post/magma-wow-blocking-graph.png)

The website of **www.womenonweb.org** is being blocked in the following
networks: Vodafone (AS12357 and AS12430), Vodafone Ono (AS6739), Orange (AS12479
and AS12715), CSUC (AS13041), MÁSMÓVIL (AS15704), XFERA Móviles (AS16299),
Telefónica/Movistar (AS3352).  In the following sections we are going to analyze
how ISPs are blocking the website.

## DNS Manipulation

We have found that the ISPs Orange (AS12715 and AS12479), XFERA Móviles
(AS16299), Telefónica (AS3352), and MÁSMÓVIL (AS15704) are blocking access to
**www.womenonweb.org**'s website by means of DNS tampering.

The ISPs Telefónica (AS3352) and Orange (AS12715 and AS12479) are blocking the
website by hijacking the domain name and pointing its DNS (A record) to the IP
address `127.0.0.1`.  This IP address is assigned for use as the Internet host
loopback address and such IP addresses should not appear on any network anywhere
(according to [RFC 1700](https://tools.ietf.org/html/rfc1700#page-5)).

In a similar way MÁSMÓVIL (AS15704) and XFERA (AS16299) are blocking the website
by hijacking the domain name of **womenonweb.org** to falsely point to the IP
address `192.168.1.254`, which belongs to a private address space. Typically
this is an IP address for a home or small office private network and should
never be used for a public web server or an online service as it cannot be
routed through the public internet. In any case, this is not the IP address of
**www.womenonweb.org**.

In these two cases of DNS manipulation a visitor of the website will not see a
block page or any information on why the website has been blocked and is not
accessible.  Visitors of the website from Orange and Telefónica may falsely
understand that there is a technical problem with the website and not that it
has been blocked by their ISP.

Here we show the latest network measurements on all the ISPs, indicating
evidence of theblocking of the website **womenonweb.org** via DNS tampering.

| ASN   |  ISP  | Blocked website | OONI report | Blocking method |
| ------- | --- | --------------- | ----------- | --------------- |
| AS12715 | Orange | www.womeonweb.org | [2020-04-24 17:49:32](https://explorer.ooni.org/measurement/20200424T145114Z_AS12715_n957DOvcjQK3WrrzLhWOE9N42XOiGpgG4bXfQlIH7DyvSZFhkb?input=https%3A%2F%2Fwww.womenonweb.org%2F) | DNS tampering |
| AS12479 | Orange | www.womeonweb.org | [2020-04-25 19:42:54](https://explorer.ooni.org/measurement/20200425T194254Z_AS12479_98fzUv7RFThoDIc7K72nCdHe285jqa4kEpKJeP0I5pqqWRh4Ve?input=https%3A%2F%2Fwww.womenonweb.org%2F) | DNS tampering |
| AS16299 | XFERA  | www.womeonweb.org | [2020-04-25 15:01:30](https://explorer.ooni.org/measurement/20200425T150131Z_AS16299_Kbadm4VFQbQmYZ6eJ4dbTB4Qa7nOhsTbGzRSNoOpHgU1qJRfYN?input=https%3A%2F%2Fwww.womenonweb.org%2F) | DNS tampering |
| AS3352  | Telefónica | www.womeonweb.org | [2020-02-23 12:33:58](https://explorer.ooni.org/measurement/20200223T110118Z_AS3352_M8eEoriCEVeLL1ZyNWJs0lEXxhHk1DyCgndDBMXKUB6tUqa9IE?input=http%3A%2F%2Fwww.womenonweb.org%2F) | DNS tampering |
| AS15704 | MÁSMÓVIL | www.womeonweb.org | [2020-04-25 08:17:30](https://explorer.ooni.org/measurement/20200425T081730Z_AS15704_O8ELT6oqEFwzfJQxqu3lF9l4iRVVujgj1dTCHfAwL5Z0H7dgYk?input=http%3A%2F%2Fwomenonweb.org%2F) | DNS tampering |

## Deep Packet Inspection (DPI)

Out of the many techniques ISPs can censor websites, DPI is the basis of the
most advanced form of censorship. Usually ISPs are implementing censorship by
manipulating the DNS records of the websites in question. Some ISPs, however,
use more invasive technologies to censor websites—DPI. DPI technologies are
often times used by ISPs to perform surveillance or to intercept network
communications of their users, which is not possible with simpler DNS
manipulation.

Special appliances have the ability not only to look into network layer 3 or
layer 4 headers but to also look inside the payload of each and every packet.
They can distinguish packets going to a server and either stop them from
reaching their target, change the server's response, or even redirect the
packets to another server. These devices perform a hostile, active,
middle-person (MITM) attack on every client connecting to the network through
them.

During our research we have identified 2 different DPI companies:
[Fortinet](https://www.fortinet.com) in Telefónica's network and
[Allot](https://allot.com) in Vodafone's network. Both have been used to
actively manipulate users' network traffic and block the websites of
**womenonweb.org**.

### HTTP Blocking

#### Movistar with Fortinet, overview

AS3352, owned by Movistar (a company owned by Telefónica), has been found to
intercept the network communication of its users to display a phony website that
displays an `HTTP 404 error`, i.e., a status code of web servers for announcing
that a specific page doesn't exist.  However this is not the case as we see in
the control measurements of the OONI Web Connectivity tests that have been
collected at the same time as the measurements on the AS3352 network.

Moreover, if we look at the HTML body of the HTTP response we can find further
evidence that this ISP is using DPI to censor the website
**www.womenonweb.org**. From the HTML body listed below we can infer from the
string `FGT_HOSTNAME` that this ISP is using a DPI product from Fortinet called
Fortigate. This is confirmed by [Fortinet's own help
pages](https://web.archive.org/web/20200502110551/https://help.fortinet.com/fos50hlp/52data/
Content/FortiOS/fortigate-whats-new-52/securityprofiles.htm). More specifically,
this string also mentions the Fortigate unique hostname used `RFFBTB1-01` by
this DPI.

Searching online for this unique hostname identifier `RFFBTB1-01` we can find a
support request to Movistar's community helpdesk with the title 'Bloqueo de
pagina web' (translated to English as: `Block of a website`). A user on
[Movistar's community
helpdesk](https://web.archive.org/web/20200409120713/https://comunidad.movistar.es/t5/
Soporte-Fibra-y-ADSL/Bloqueo-de-pagina-web/td-p/3963231) was asking why the
website `http://www.argenteam.net/` is being blocked. Reading throughout the
post we were able to find verbatim the same block page (`ERROR 404 - File not
found`) as the one found to block the website of Women on Web. Moreover the
presence of the hostname (`FGT_HOSTNAME: RFFBTB1-01`) in Movistar's helpdesk
suggests that other websites are being blocked from Movistar networks with the
same methodology.

Additionally the same block page by Movistar has been found in an OONI network
measurement from 2018 showing the [blocking of **thepiratebay.org**
website](https://explorer.ooni.org/measurement/20180826T220429Z_AS3352_3keVVUGxBWS
TeAwH74fpvgL9wviZtkL6dmqRkcmZezRo3RuU0f?input=http://thepiratebay.org).

#### Movistar block page with Fortinet

Movistar uses the following block page to censor access to the website of Women
on Web:

```
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
<!--
CATEGORY:
DEST_IP:  67.213.76.19
FGT_HOSTNAME:  RFFBTB1-01
SOURCE_IP:  [REDACTED]
-->
<html>
  <head>
    <title id="3">
      Error 404
    </title>
  </head>
  <body>
    <CENTER>
      <h1>
        ERROR 404 - File not found
      </h1>
    </CENTER>
  </body>
</html>
```

The complete block page and technical evidence of the blocking can be found in
detail in [OONI's network
measurement](https://explorer.ooni.org/measurement/20200417T083134Z_AS3352_sD1AccbN0vPsT0uG3LdMMaKe5IvHoYDCTiTRRge89TYTen24Du?input=http%3A%2F%2Fwww.womenonweb.org%2F).

From different vantage points we were able to identify two almost identical
block pages with different Fortigate hostnames set as `RFFBTB1-02` and
`RFFMNO1-01`. Additionally the title HTML tag of the block page seems to be a
different one per server/hostname `id="1"` and `id="4"`. Based on their
configuration structure and their hostname naming these are probably different
DPI servers operated by the same ISP.

```
|  1 <!DOCTYPE html PUBLIC "-//W3C//DTD |  1 <!DOCTYPE html PUBLIC "-//W3C//DTD |  1 <!DOCTYPE html PUBLIC "-//W3C//DTD
|  2 <!--                               |  2 <!--                               |  2 <!--
|  3 CATEGORY:                          |  3 CATEGORY:                          |  3 CATEGORY:
|  4 DEST_IP:  67.213.76.19             |  4 DEST_IP:  67.213.76.19             |  4 DEST_IP:  67.213.76.19
#  5 FGT_HOSTNAME:  RFFMNO1-02          #  5 FGT_HOSTNAME:  RFFBTB1-01          #  5 FGT_HOSTNAME:  RFFBTB1-02
|  6 SOURCE_IP:  [REDACTED]             |  6 SOURCE_IP:  [REDACTED]             |  6 SOURCE_IP:  [REDACTED]
|  7 -->                                |  7 -->                                |  7 -->
|  8 <html>                             |  8 <html>                             |  8 <html>
|  9   <head>                           |  9   <head>                           |  9   <head>
# 10     <title id="4">                 # 10     <title id="3">                 # 10     <title id="4">
| 11       Error 404                    | 11       Error 404                    | 11       Error 404
| 12     </title>                       | 12     </title>                       | 12     </title>
| 13   </head>                          | 13   </head>                          | 13   </head>
| 14   <body>                           | 14   <body>                           | 14   <body>
| 15     <CENTER>                       | 15     <CENTER>                       | 15     <CENTER>
| 16       <h1>                         | 16       <h1>                         | 16       <h1>
| 17         ERROR 404 - File not found | 17         ERROR 404 - File not found | 17         ERROR 404 - File not found
| 18       </h1>                        | 18       </h1>                        | 18       </h1>
| 19     </CENTER>                      | 19     </CENTER>                      | 19     </CENTER>
| 20   </body>                          | 20   </body>                          | 20   </body>
| 21 </html>                            | 21 </html>                            | 21 </html>
 ```

Comparison of all three Fortinet's Fortigate DPI block pages in parallel. In
line 6 there are 3 unique Fortigate hostnames. The OONI network measurements
revealing Movistar's block pages with all technical details can be found
[here][fgt1], [here][fgt2] and [here][fgt3].

[fgt1]: https://explorer.ooni.org/measurement/20200417T083134Z_AS3352_sD1AccbN0vPsT0uG3LdMMaKe5IvHoYDCTiTRRge89TYTen24Du?input=http%3A%2F%2Fwww.womenonweb.org%2F
[fgt2]: https://explorer.ooni.org/measurement/20200303T151517Z_AS3352_xjqLbIT6crZPgO8WnIk0Fmhegi1JOmdzSxavclcDlxihSjqJf3?input=http%3A%2F%2Fwww.womenonweb.org%2F
[fgt3]: https://explorer.ooni.org/measurement/20200416T100037Z_AS3352_olwxGpXODPSWP8Dd2kvkGg5yw7YYTtZALgjT9UXo5u9emcRJ6X?input=http%3A%2F%2Fwww.womenonweb.org%2F

A few OONI network measurements from Consorci de Serveis Universitaris de
Catalunya (CSUC) reveal the same block page with Telefónica, as CSUC uses
Telefónica as their upstream ISP.

| ASN   |  ISP  | Blocked website | OONI report | Blocking method |
| ------- | --- | --------------- | ----------- | --------------- |
| AS3352  | Telefónica | www.womeonweb.org | [2020-04-17 08:31:33](https://explorer.ooni.org/measurement/20200417T083134Z_AS3352_sD1AccbN0vPsT0uG3LdMMaKe5IvHoYDCTiTRRge89TYTen24Du?input=http%3A%2F%2Fwww.womenonweb.org%2F) | HTTP blocking (DPI) |
| AS13041 | CSUC | www.womeonweb.org | [2020-02-18 08:34:35](https://explorer.ooni.org/measurement/20200218T120422Z_AS13041_nhU6KWkhiZFICHGeZ1uEiEZXEGguyfMdvNAzgTdi0I8ZifAwSI?input=http%3A%2F%2Fwww.womenonweb.org%2F) | HTTP blocking (DPI) |

#### Vodafone block page with Allot

Users of the Vodafone ISP (AS12357 and AS12430) are shown the following generic
block page when they visit (the HTTP version) of the website **womeonweb.org**.

This block page is another indication that Vodafone blocks the website.

![Block page: "Por causas ajenas a Vodafone, esta web no está disponible"](https://escrever.coletivos.org/uploads/upload_88c2a2023f4fadc7ffb81841bea8e85f.png)


Vodafone's block page text in original language (Spanish):
> Por causas ajenas a Vodafone, esta web no está disponible

The block page, translated in English, means:
> For reasons beyond Vodafone's control, this website is not available

### TLS Interception

Vodafone ISP (AS 12357 and AS 12430), like Movistar, is using a technique
described in network security called “[middle-person
attack](https://en.wikipedia.org/wiki/Middleperson_attack)". Vodafone does not
reset the TLS connection during the TLS handshake, like Movistar does. Rather,
with AS 12357 and AS 12430, the TLS handshake terminates and the user receives a
forged certificate claiming that it belongs to the www.womenonweb.org website.

| ASN   |  ISP  | Blocked website | OONI report | Blocking method |
| ------- | --- | --------------- | ----------- | --------------- |
| AS12357 | Vodafone | www.womeonweb.org | [2020-04-23 15:20:11](https://explorer.ooni.org/measurement/20200423T152014Z_AS12357_gchrxfK3WJiFUvhBYPJKtkOYdHk0CezF09oYdqr06YRJusuGJu?input=https%3A%2F%2Fwww.womenonweb.org%2F) | TLS interception |
| AS12430 | Vodafone | www.womeonweb.org | [2020-04-25 19:41:43](https://explorer.ooni.org/measurement/20200425T194144Z_AS12430_0AFqAsjxCgEqTuLFFJLAbWOBgAGXCdn4Ijybxpt1KJod9hftol?input=https%3A%2F%2Fwww.womenonweb.org%2F) | TLS interception |
<br>

Several network measurements from the Vodafone ISP (AS 12357 and AS 12430)
present a certificate verification failure (`ssl_error: error:14007086:SSL
routines:CONNECT_CR_CERT:certificate verify failed`) indicating that TLS
interception is probably being deployed on the network for the website
**www.womenonweb.org**.This error message from the OpenSSL library indicates
that the TLS handshake is over and the client is not able to verify the
certificate provided by the server.

In further tests using the OpenSSL command line tool, we confirm the TLS
interception for connections to Women on Web's web server, given some conditions
that we detail below.

#### Forged TLS certificate

Once a TCP connection is established (we don't have a TCP reset here), browsers
send a TLS `Client Hello` message to the web server as the first step to
establish an encrypted and authenticated channel. We reproduce it here with the
OpenSSL command line tool:

```
> openssl s_client -connect 67.213.76.19:443 -servername www.womenonweb.org  < /dev/null |& egrep 'issuer|subject'
subject=CN = www.womenonweb.org
issuer=C = ES, ST = Madrid, L = Madrid, O = Allot, OU = Allot, CN = allot.com/emailAddress=info@allot.com
```

What the OpenSSL command does:
1. `s_client`: Uses the integrated TLS client
2. `-connect …`: Connects to Women on Web IP address on HTTPS port (443)
3. `-servername …`: Indicates the hostname we want to access
4. `< /dev/null`: Closes the connection once established
5. `|& egrep …`: Filters everything out except issuer and subject details from the received server certificate

The result of the previous command clearly shows us a response presenting a
forged TLS certificate, claiming to be for www.womenonweb.org (subject's Common
Name), and issued by Allot, which by no means is a recognized Certificate
Authority.

As OONI tests do not save the TLS certificates that servers reply, our team uploaded it for
[public
inspection](https://github.com/ooni/probe-cli/files/4567648/www_womenonweb_org.crt.txt). The
forged certificate of Vodafone has an issue date of 27th January 2019, one year before the start
of our data analysis that showed signs of blocking.

![Screen capture of Chromium browser showing the forged certificate for www.womenonweb.org](https://escrever.coletivos.org/uploads/upload_a670dce967501e61a8f186720ca5e374.png)


#### Matching on SNI

An important detail here is the `-servername` parameter that we added. It
controls the "Server Name Indication" (SNI) extension of TLS, sent inside the
`ClientHello` message. It is sent unencrypted from the client to the server, and
its intended use is to help web servers that host multiple TLS enabled sites
(HTTPS) at the same IP address, to be able to reply back a `ServerHello` message
with the certificate corresponding to the desired website, as each site may have
its own certificate.

However, this also poses a risk. As the SNI field is unencrypted, it is being
used by some censoring systems to identify and intercept connections to domains
they want to block.  Therefore, we tried to repeat the connection leaving the
SNI out, by using the `-noservername` option of the OpenSSL tool. This is done
using the command below:

```
> openssl s_client -connect 67.213.76.19:443 -noservername < /dev/null |& egrep 'issuer|subject'
subject=CN = womenonweb.org
issuer=C = US, O = Let's Encrypt, CN = Let's Encrypt Authority X3
```

The shortened reply shows us a certificate issued for `womenonweb.org` (notice
the lack of `www.`), and signed by Let's Encrypt certificate authority. Although
this output in itself doesn't demonstrate that the certificate is valid, we
downloaded it and verified that it is. In fact, it is the same certificate
returned by `https://67.213.76.19/` in non-blocked networks. It looks like the
web server's default HTTPS website is `womenonweb.org`, which only contains a
redirect to `www.womenonweb.org`. It would be interesting to see what would
happen if the default HTTPS website had been `www.womenonweb.org` instead of the
one with the content.

#### Differences with "SNI Blocking"

Even though Allot DPI (operating in Vodafone's networks) is using SNI to match a
packet, the SNI alone is not enough to trigger the blocking. Therefore, the
technique called SNI blocking and described in a recent [OONI blog
post](https://ooni.org/post/2020-iran-sni-blocking/) would not be enough to
detect this kind of interference. We contacted OONI developers and we
collaborated with them to test a new measurement methodology designed to
automatically gather all the information required to analyze this specific
scenario.

The following OpenSSL command shows how the SNI blocking technique is
ineffective in this use case:

```
> openssl s_client -connect wikipedia.org:443 -servername womenonweb.org  < /dev/null |& egrep 'issuer|subject'
subject=C = US, ST = California, L = San Francisco, O = "Wikimedia Foundation, Inc.", CN = *.wikipedia.org
issuer=C = US, O = DigiCert Inc, OU = www.digicert.com, CN = DigiCert SHA2 High Assurance Server CA
```

What the experiment above shows is that even if we indicate a targeted
servername in the SNI, we can establish a TLS connection to an unblocked web
server (Wikipedia's in this case) without interception.

#### Domain and subdomain variations

Regarding the redirections, both HTTP and HTTPS versions of `womenonweb.org`,
that is, without `www.` prefix or subdomain, are not blocked at Vodafone
networks. In the case of TLS, we don't see any forged certificate:

```
> openssl s_client -connect 67.213.76.19:443 -servername womenonweb.org  < /dev/null |& egrep 'issuer|subject'
subject=CN = womenonweb.org
issuer=C = US, O = Let's Encrypt, CN = Let's Encrypt Auhority X3
```

However, this domain only redirects to `https://www.womenonweb.org`, which is
then intercepted.

#### Replacement block page

Finally, the content that Vodafone presents to us is similar but not identical
to the webpage returned in the HTTP version, as explained in the HTTP blocking
section above.

Trying to access the HTTP version, unencrypted, we receive:

```
 > curl http://www.womenonweb.org
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<META HTTP-EQUIV="Expires" CONTENT="-1">
<html>Por causas ajenas a Vodafone, esta web no est� disponible</html>
```
*(line feed added manually above for readability)*

However, trying to access the HTTPS version, and disregarding the false certificate using the `--insecure` option, we receive:

```
curl --insecure https://www.womenonweb.org
<html><body><p>Por causas ajenas a Vodafone, esta web no est� disponible</body></html>
```
This simple webpage differs from the first one in that it lacks the `META` tags,
but wraps its content in a `body` tag, and opens an inner `p` tag without a
corresponding close tag.

### TCP Reset

A TCP reset attack is a way to tamper and terminate an Internet connection by
sending a forged TCP reset packet ([Wikipedia: TCP reset
attack](https://en.wikipedia.org/wiki/TCP_reset_attack)).

The
[`response_never_received`](https://twistedmatrix.com/documents/15.4.0/api/twisted.web._newclient.ResponseNeverReceived.html)
and `ECONNRESET` mean that the other side of the TCP conversation abruptly
closed its end of the connection. Indicating a potential TCP reset attack.

| ASN   |  ISP  | Blocked website | OONI report | Blocking method |
| ------- | --- | --------------- | ----------- | --------------- |
| AS6739  | Vodafone | www.womeonweb.org | [2020-03-08 07:01:48](https://explorer.ooni.org/measurement/20200312T120013Z_AS6739_mUP548jcZP5NcgCRlvQnQzj62GdhFPrsMW5fegMKHqAA5Lgl4C?input=https%3A%2F%2Fwww.womenonweb.org%2F) | TCP reset (`response_never_received`) |
| AS3352  | Movistar | www.womeonweb.org | [2020-04-23 04:36:10](https://explorer.ooni.org/measurement/20200423T043611Z_AS3352_AFUmdkbXN0SBIhvCt41wknshmPLDH0KytEARzU19A5lzAUsNpo?input=https%3A%2F%2Fwww.womenonweb.org%2F) | TCP reset (`response_never_received`) |
| AS3352  | Movistar | www.womeonweb.org | [2020-04-25 22:07:44](https://explorer.ooni.org/measurement/20200425T220745Z_AS3352_zNqCGG3OHh8ozsChS65rCmplMNnGUKh3E53WsfnuLxg6VBZnvZ?input=https%3A%2F%2Fwww.womenonweb.org%2F)| TCP reset (`ECONNRESET`) |
| AS13041 | CSUC | www.womeonweb.org | [2020-02-19 18:57:37](https://explorer.ooni.org/measurement/20200221T091617Z_AS13041_arT1Z8qkTHPJqenbSmeJuiaXrSsNnIGhGW4HHtMbYL6RlCDobU?input=https%3A%2F%2Fwww.womenonweb.org%2F) | TCP reset (`response_never_received`) |

It's worth mentioning that later tests from AS6739 show another blocking
strategy, consistently over time, suggesting that between the [16th of March
2020](https://explorer.ooni.org/measurement/20200318T120002Z_AS6739_2Wdqcmjt1pcGvmPWq9uu3b6riNc4fVxETPC6Wogt3QKJkvEg8l?input=https%3A%2F%2Fwww.womenonweb.org%2F)
and the [24th of April
2020](https://explorer.ooni.org/measurement/20200424T173934Z_AS6739_3z3DVCb57D2AwYkAbZMmDbsOXyAAQCF4zDDWHRGzhuElQ4BLpj?input=https%3A%2F%2Fwww.womenonweb.org%2F),
Vodafone moved from a simpler to a more complex strategy, at least at this
network (AS6739).

## DPI Circumvention

During our research we encountered [Qurium's
article](https://www.qurium.org/alerts/spain/blocking-techniques-catalunya/)
about the technical mechanisms used to block the websites related to the Catalan
referendum in October 2017. We were able to circumvent the DPI blocking with the
exact same method.

Specifically the DPI system keeps its session state for 10 seconds. Thus, by
delaying the transmission of the HTTP GET request (`"GET / HTTP/1.1"`) we can
successfully circumvent the DPI, since the TCP session is not being tracked
after 10 seconds. The following command allows us to circumvent the DPI on the
Vodafone ISP (AS 12357 and AS 12430) that uses the DPI infrastructure from
Allot.

```
input () {
  sleep 20
  echo "GET / HTTP/1.1"
  echo "Host: www.womenonweb.org"
  echo
  echo
}

input | nc www.womenonweb.org 80
```

Another strategy [published in
2013](https://ooni.org/post/tab-tab-come-in/#how-we-manage-to-bypass-the-blocking)
by OONI takes advantage of the HTTP header sanitization process that web servers
perform, in contrast to the lack of it in DPI systems.

Adapting the previous command to exploit this strategy also proved successful:

```
input () {
  echo "GET / HTTP/1.1"
  echo -e "Host: www.womenonweb.org\t"
  echo
  echo
}

input | nc www.womenonweb.org 80
```

In both cases, upon DPI circumvention the response is an HTTP to HTTPS
redirection. This is expected and is a standard practice to redirect users to
the HTTPS version of **www.womenonweb.org** (in an uncensored connection):

```
HTTP/1.1 302 Found
Cache-Control: no-cache
Content-length: 0
Location: https://www.womenonweb.org/
Connection: close
```

## Conclusions

Our technical analysis of OONI data collected by multiple volunteers during the
period January 1st to April 30th of 2020 revealed consistent blocking of Women
On Web's website (**www.womenonweb.org**). We found evidence of blocking in 9
networks used by 5 major broadband and mobile ISPs in Spain.

We were able to verify the usage of DPI technology from Fortinet and Allot used
by Telefónica and Vodafone to block access to the website. Furthermore we have
detected 2 different types of block pages in the same networks.

Based on evidence from network measurements analyzed in this article we were
able to verify the blocking of Women on Web website by means of DNS
Manipulation, HTTP Blocking, TLS Interception and TCP reset.

These methods are by no means exclusive for the censorship of this website, they
seem to be used routinely as shown by the regular reports published by OONI.

## Censorship circumvention

Our findings in Spain revealed censorship of the **womenonweb.org** website with
DNS tampering, DPI, TLS interception, HTTP blocking, and TCP resets.

In the case of DNS tampering you may be able to circumvent the censorship and
access the website by changing the DNS resolver(s).

However we found that in some networks the ISPs have been deploying DPI blocking
and DNS tampering, and in theses cases changing of the DNS resolver(s) may not
be adequate to circumvent the censorship.

You can bypass the censorship and blocking of the website with the use of [Tor
Browser](https://www.torproject.org/download/).

## Previous coverage

* [Catalan] [Donestech: Censura a l'estat espanyol de la web womenonweb.org](https://donestech.net/noticia/censura-lestat-espanyol-de-la-web-womenonweborg)
* [Spanish] [Freakspot: Censura de sitio web de Women on Web en España](https://freakspot.net/censura-de-p%C3%A1gina-web-al-aborto-espa%C3%B1a/)
* [Spanish] [Ana: Women on Web](https://network23.org/ana/ooni-women-on-web/)

## Acknowledgement

### Contributors and testers
Many ideas, discoveries, and testing across networks and time must be credited
to:

- Hackmeeting mailing list community
- Sindominio's cafe chat community
- OONI core and community members
- Calbasi
- And many, many friends, mates, relatives, and anonymous testers

### Supporting collectives

This work wouldn't have been possible without the supporting infrastructure of:

- Sindominio.net
- Indymedia.org
- Riseup.net
- Aktivix.org
- Coletivos.org

## References

- [Fortinet - Security Profiles](https://web.archive.org/web/20200502110551/https://help.fortinet.com/fos50hlp/52data/Content/FortiOS/fortigate-whats-new-52/securityprofiles.htm)
- [Movistar community helpdesk](https://web.archive.org/web/20200409120713/https://comunidad.movistar.es/t5/Soporte-Fibra-y-ADSL/Bloqueo-de-pagina-web/td-p/3963231)
- [OONI - On the blocking of abortion rights websites: Women on Waves & Women on Web](https://ooni.org/post/2019-blocking-abortion-rights-websites-women-on-waves-web/)
- [OONI - Tab tab, come in!](https://ooni.org/post/tab-tab-come-in/#how-we-manage-to-bypass-the-blocking)
- [OONI - Web Connectivity test](https://ooni.org/nettest/web-connectivity/)
- [Qurium - Blocking Techniques Catalunya](https://www.qurium.org/alerts/spain/blocking-techniques-catalunya/)
- [RFC 1700](https://tools.ietf.org/html/rfc1700)
- [Tor](https://www.torproject.org/)
- [Wikipedia - MITM attack](https://en.wikipedia.org/wiki/middle-person_attack)
- [Wikipedia - TCP reset attack](https://en.wikipedia.org/wiki/TCP_reset_attack)
